#!/bin/bash

set -eux

dnf install -y dnf-plugins-core
dnf copr enable -y packit/osbuild-osbuild-composer-3077
dnf copr enable -y @osbuild/osbuild
dnf install -y osbuild-composer lorax weldr-client s3cmd

systemctl enable --now osbuild-composer.socket

tee "bp.toml" >/dev/null <<EOF
name = "bp"
distro = "fedora-38"
EOF

composer-cli blueprints push bp.toml
composer-cli --json compose start bp image-installer >compose-status.json

COMPOSE_ID=$(jq -r '.[0].body.build_id' compose-status.json)

while true; do
  sudo composer-cli --json compose info "${COMPOSE_ID}" | tee compose-info.json >/dev/null
  COMPOSE_STATUS=$(jq -r '.[0].body.queue_status' compose-info.json)

  # Is the compose finished?
  if [[ $COMPOSE_STATUS != RUNNING ]] && [[ $COMPOSE_STATUS != WAITING ]]; then
    break
  fi

  # Wait 30 seconds and try again.
  sleep 30
done

if [[ $COMPOSE_STATUS != FINISHED ]]; then
    echo "Something went wrong with the compose. 😢"
    composer-cli compose logs "$COMPOSE_ID"
    tar xf "$COMPOSE_ID-logs.tar"
    cat logs/*
    exit 1
fi

FILENAME="${COMPOSE_ID}-installer.iso"

composer-cli compose image "${COMPOSE_ID}"

curl -Lo payload.tar.gz https://fedorapeople.org/groups/anaconda/webui/payload/payload.tar.gz
gunzip payload.tar.gz
mv payload.tar liveimg.tar

RESULT="fedora-preview-installer-$(date --iso-8601=seconds).iso"
mkksiso -a liveimg.tar "$FILENAME" "$RESULT"

if [ "${CI_COMMIT_BRANCH:-}" == "main" ]; then
  URL_ENCODED_RESULT="$(echo -n "$RESULT" | jq -rR @uri)"
  AWS_ACCESS_KEY_ID="$S3_KEY_ID" \
    AWS_SECRET_ACCESS_KEY="$S3_SECRET_KEY" \
    s3cmd --acl-public put "$RESULT" "s3://fedora-preview-installer-images/$RESULT"

  echo "The resulting artifact is at:"
  echo "https://fedora-preview-installer-images.s3.amazonaws.com/$URL_ENCODED_RESULT"
fi
